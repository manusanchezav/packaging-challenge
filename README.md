# PACKAGING-CHALLENGE #

A library that optimize the items you can put inside a package.

### Local Configuration ###

To compile the project

    mvn clean install

### Brief explanation of decisions ###

As this was meant to be a library I decided to not use any Dependency Injection library such as Spring or CDI, I think a library should be agnostic of any of these 
because probably in the future a new application will be using a newer version and there could be compatibility issues.

I decided to use the Factory design pattern to take out the responsibility of instantiation of a class from client program to the factory class.

I also decided to have Validators with their own Validation Exception, and a common Validation Command that is going to be in charge of calling all the possible validations.

I created different mappers for Package and Item which functions are to apply a transformation from string to pojo and from pojo to string.

I decided to have interfaces and implementations of the service layer(Packer, Package and File), to achieve total abstraction and loose coupling. (Similar as Spring standards).