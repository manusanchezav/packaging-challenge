package com.mobiquity.exception;

/**
 * The type Api exception.
 */
public class APIException extends Exception {

  /**
   * Instantiates a new Api exception.
   *
   * @param message the message
   * @param e       the e
   */
  public APIException(String message, Exception e) {
    super(message, e);
  }

}