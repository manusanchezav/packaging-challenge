package com.mobiquity.packer;

import com.mobiquity.exception.APIException;
import com.mobiquity.packer.factory.PackerServiceFactory;
import com.mobiquity.packer.service.PackerService;

/**
 * The type Packer.
 */
public class Packer {

  private static final PackerService PACKER_SERVICE = PackerServiceFactory.create();

  /**
   * Main Pack method.
   *
   * @param filePath the file path
   * @return the string output with the pack result
   * @throws APIException the api exception
   */
  public static String pack(String filePath) throws APIException {
    try {
      return PACKER_SERVICE.pack(filePath);
    } catch (Exception exception) {
      throw new APIException(exception.getMessage(), exception);
    }
  }
}
