package com.mobiquity.packer.factory;

import com.mobiquity.packer.validator.Validator;
import com.mobiquity.packer.validator.ValidatorCommand;
import com.mobiquity.packer.validator.impl.MaxItemWeightAndPriceValidator;
import com.mobiquity.packer.validator.impl.MaxItemsValidator;
import com.mobiquity.packer.validator.impl.MaxPackageWeightValidator;

import java.util.Arrays;

/**
 * The type Validator factory.
 */
public class ValidatorFactory {

  /**
   * Create validator Command with all the different validators.
   *
   * @return the validator
   */
  public static Validator create() {
    return new ValidatorCommand(Arrays.asList(new MaxPackageWeightValidator(), new MaxItemsValidator(), new MaxItemWeightAndPriceValidator()));
  }
}
