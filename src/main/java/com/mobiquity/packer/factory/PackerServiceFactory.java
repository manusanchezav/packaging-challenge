package com.mobiquity.packer.factory;

import com.mobiquity.packer.mapper.PackageMapper;
import com.mobiquity.packer.service.PackerService;
import com.mobiquity.packer.service.impl.FileServiceImpl;
import com.mobiquity.packer.service.impl.PackageServiceImpl;
import com.mobiquity.packer.service.impl.PackerServiceImpl;

/**
 * The type Packer service factory.
 */
public class PackerServiceFactory {

  /**
   * Create packer service with all the implementations.
   *
   * @return the packer service
   */
  public static PackerService create() {
    return new PackerServiceImpl(new FileServiceImpl(), new PackageServiceImpl(), ValidatorFactory.create(), new PackageMapper());
  }
}
