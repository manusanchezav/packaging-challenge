package com.mobiquity.packer.mapper;

import com.mobiquity.packer.model.Item;
import org.apache.commons.lang3.StringUtils;

import java.util.function.Function;

/**
 * The type Item mapper.
 */
public class ItemMapper implements Function<String, Item> {

  private static final String EURO = "€";
  private static final String REGEX = "([()])";
  private static final String ITEM_VALUE_SEPARATOR = ",";
  private static final int ITEM_INDEX = 0;
  private static final int WEIGHT_INDEX = 1;
  private static final int PRICE_INDEX = 2;

  /**
   * Instantiates a new Item mapper.
   */
  public ItemMapper() {
  }

  /**
   * Apply a string to object mapping of the item.
   *
   * @param item the item
   * @return the object representation of the item.
   */
  @Override
  public Item apply(String item) {
    String[] itemValues = item.replaceAll(REGEX, StringUtils.EMPTY).split(ITEM_VALUE_SEPARATOR);
    Integer index = Integer.valueOf(itemValues[ITEM_INDEX]);
    Double weight = Double.valueOf(itemValues[WEIGHT_INDEX]);
    Double price = Double.valueOf(itemValues[PRICE_INDEX].replace(EURO, StringUtils.EMPTY));
    return new Item(index, weight, price);
  }

  /**
   * Apply an object to string mapping of the item.
   *
   * @param item the item
   * @return the string representation of the item.
   */
  public String applyToString(Item item) {
    return String.valueOf(item.getIndex());
  }
}