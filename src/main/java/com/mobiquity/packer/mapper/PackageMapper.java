package com.mobiquity.packer.mapper;

import com.mobiquity.packer.model.Item;
import com.mobiquity.packer.model.Package;
import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * The type Package mapper.
 */
public class PackageMapper implements Function<String, Package> {

  private static final String PACKAGE_WEIGHT_AND_ITEMS_SEPARATOR = ":";
  private static final String NO_ITEMS = "-";
  private static final String ITEMS_JOINER = ",";
  private static final int WEIGHT_INDEX = 0;
  private static final int ITEMS_INDEX = 1;

  private final ItemMapper itemMapper = new ItemMapper();

  /**
   * Instantiates a new Package mapper.
   */
  public PackageMapper() {
  }

  /**
   * Apply a string to object mapping of the package.
   *
   * @param aPackage the package
   * @return the object representation of the package.
   */
  @Override
  public Package apply(String aPackage) {
    String[] packageWeightAndItems = aPackage.split(PACKAGE_WEIGHT_AND_ITEMS_SEPARATOR);

    Double packageWeight = Double.valueOf(packageWeightAndItems[WEIGHT_INDEX].trim());
    String packageItems = packageWeightAndItems[ITEMS_INDEX].trim();

    List<Item> items = Arrays.stream(packageItems.split(StringUtils.SPACE))
        .map(itemMapper)
        .collect(Collectors.toList());
    return new Package(packageWeight, items);
  }

  /**
   * Apply an object to string mapping of the package.
   *
   * @param aPackage the package
   * @return the string representation of the package.
   */
  public String applyToString(Package aPackage) {
    List<Item> items = aPackage.getItems();
    return items.isEmpty() ? NO_ITEMS : items.stream().map(itemMapper::applyToString).collect(Collectors.joining(ITEMS_JOINER));
  }
}