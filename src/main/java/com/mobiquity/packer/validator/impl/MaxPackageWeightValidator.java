package com.mobiquity.packer.validator.impl;

import com.mobiquity.packer.exception.ValidationException;
import com.mobiquity.packer.model.Package;
import com.mobiquity.packer.model.PackerConstants;
import com.mobiquity.packer.validator.Validator;

/**
 * The type Max package weight validator.
 */
public class MaxPackageWeightValidator implements Validator {

  private static final String MAX_PACKAGE_WEIGHT_EXCEPTION = "The weight exceeded the max weight that a package can take";

  /**
   * Instantiates a new Max package weight validator.
   */
  public MaxPackageWeightValidator() {
  }

  /**
   * Validates that the package doesnt exceeds the max weight
   *
   * @param aPackage the package
   * @throws ValidationException the validation exception
   */
  @Override
  public void validate(Package aPackage) throws ValidationException {
    if (aPackage.getMaxWeight() > PackerConstants.MAX_WEIGHT) {
      throw new ValidationException(MAX_PACKAGE_WEIGHT_EXCEPTION);
    }
  }
}
