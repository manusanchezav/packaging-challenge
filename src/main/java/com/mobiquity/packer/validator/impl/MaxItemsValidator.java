package com.mobiquity.packer.validator.impl;

import com.mobiquity.packer.exception.ValidationException;
import com.mobiquity.packer.model.Package;
import com.mobiquity.packer.model.PackerConstants;
import com.mobiquity.packer.validator.Validator;

/**
 * The type Max items validator.
 */
public class MaxItemsValidator implements Validator {

  private static final String MAX_ITEMS_EXCEPTION = "The number of items exceeds the amount that a package can take";

  /**
   * Instantiates a new Max items validator.
   */
  public MaxItemsValidator() {
  }

  /**
   * Validates that the package does not exceeds the max items.
   *
   * @param aPackage the package
   * @throws ValidationException the validation exception
   */
  @Override
  public void validate(Package aPackage) throws ValidationException {
    if (aPackage.getItems().size() > PackerConstants.MAX_ITEMS) {
      throw new ValidationException(MAX_ITEMS_EXCEPTION);
    }
  }
}
