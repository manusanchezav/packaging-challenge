package com.mobiquity.packer.validator.impl;

import com.mobiquity.packer.exception.ValidationException;
import com.mobiquity.packer.model.Item;
import com.mobiquity.packer.model.Package;
import com.mobiquity.packer.model.PackerConstants;
import com.mobiquity.packer.validator.Validator;

/**
 * The type Max item weight and price validator.
 */
public class MaxItemWeightAndPriceValidator implements Validator {

  private static final String MAX_ITEM_WEIGHT_EXCEPTION = "There is an item that exceeds the max weight of an item";
  private static final String MAX_ITEM_PRICE_EXCEPTION = "There is an item that exceeds the max price of an item";

  /**
   * Instantiates a new Max item weight and price validator.
   */
  public MaxItemWeightAndPriceValidator() {
  }

  /**
   * Validates that the package items do not exceed the max weight and max price
   *
   * @param aPackage the package
   * @throws ValidationException the validation exception
   */
  @Override
  public void validate(Package aPackage) throws ValidationException {
    for (Item item : aPackage.getItems()) {
      if (item.getWeight() > PackerConstants.MAX_ITEM_WEIGHT) {
        throw new ValidationException(MAX_ITEM_WEIGHT_EXCEPTION);
      }
      if (item.getPrice() > PackerConstants.MAX_ITEM_PRICE) {
        throw new ValidationException(MAX_ITEM_PRICE_EXCEPTION);
      }
    }
  }
}