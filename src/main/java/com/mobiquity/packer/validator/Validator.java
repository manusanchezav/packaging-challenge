package com.mobiquity.packer.validator;

import com.mobiquity.packer.exception.ValidationException;
import com.mobiquity.packer.model.Package;

/**
 * The interface Validator.
 */
public interface Validator {

  /**
   * Validate.
   *
   * @param aPackage the package
   * @throws ValidationException the validation exception
   */
  void validate(Package aPackage) throws ValidationException;
}
