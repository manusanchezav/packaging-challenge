package com.mobiquity.packer.validator;

import com.mobiquity.packer.exception.ValidationException;
import com.mobiquity.packer.model.Package;

import java.util.Collection;

/**
 * The type Validator command.
 */
public class ValidatorCommand implements Validator {

  private final Collection<Validator> validators;

  /**
   * Instantiates a new Validator command.
   *
   * @param validators the validators
   */
  public ValidatorCommand(Collection<Validator> validators) {
    this.validators = validators;
  }

  /**
   * Validate the package using all of the configured validators.
   *
   * @param aPackage the package
   * @throws ValidationException the validation exception
   */
  @Override
  public void validate(Package aPackage) throws ValidationException {
    for (Validator validator : validators) {
      validator.validate(aPackage);
    }
  }
}