package com.mobiquity.packer.service;

import java.io.IOException;
import java.util.List;

/**
 * The interface File service.
 */
public interface FileService {

  /**
   * Gets the lines of File from the filePath.
   *
   * @param filePath the file path
   * @return the lines
   * @throws IOException the io exception
   */
  List<String> getLines(String filePath) throws IOException;
}
