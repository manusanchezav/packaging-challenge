package com.mobiquity.packer.service.impl;

import com.google.common.collect.Sets;
import com.mobiquity.packer.model.Item;
import com.mobiquity.packer.model.Package;
import com.mobiquity.packer.service.PackageService;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * The type Package service.
 */
public class PackageServiceImpl implements PackageService {

  /**
   * Instantiates a new Package service.
   */
  public PackageServiceImpl() {
  }

  /**
   * Optimize the package by only including the items that will make
   * the total weight less than or equal to the package limit
   * and the total cost is as large as possible.
   * It will test with all the items combinations and modified the package including the optimized items.
   *
   * @param aPackage the package
   */
  @Override
  public void optimize(Package aPackage) {
    BigDecimal packageMaxWeight = BigDecimal.valueOf(aPackage.getMaxWeight());
    BigDecimal packageCurrentWeight = BigDecimal.ZERO;
    BigDecimal packageCurrentPrice = BigDecimal.ZERO;
    List<Item> optimizedItems = new ArrayList<>();
    Set<Set<Item>> itemCombinations = Sets.powerSet(aPackage.getItems().stream().collect(Collectors.toSet()));

    for (Set<Item> itemCombination : itemCombinations) {
      BigDecimal totalWeight = sumItemsWeights(itemCombination);
      if (packageMaxWeight.compareTo(totalWeight) == 1) {
        BigDecimal totalPrice = sumItemsPrice(itemCombination);
        if (isPriceHigher(packageCurrentPrice, totalPrice) || isSamePriceAndWeightsLower(packageCurrentWeight, packageCurrentPrice, totalWeight, totalPrice)) {
          optimizedItems = itemCombination.stream().collect(Collectors.toList());
          packageCurrentPrice = totalPrice;
          packageCurrentWeight = totalWeight;
        }
      }
    }

    optimizedItems.sort(Comparator.comparing(Item::getIndex));
    aPackage.setItems(optimizedItems);
  }

  /**
   * Sum the items prices
   *
   * @param itemCombination the items
   */
  private BigDecimal sumItemsPrice(Set<Item> itemCombination) {
    return itemCombination.stream()
        .map(item -> BigDecimal.valueOf(item.getPrice()))
        .reduce(BigDecimal.ZERO, BigDecimal::add);
  }

  /**
   * Sum the items weight
   *
   * @param itemCombination the items
   */
  private BigDecimal sumItemsWeights(Set<Item> itemCombination) {
    return itemCombination.stream()
        .map(item -> BigDecimal.valueOf(item.getWeight()))
        .reduce(BigDecimal.ZERO, BigDecimal::add);
  }

  /**
   * Verifies if the packageCurrentPrice is equals to totalPrice
   * and the totalWeight is lower than the packageCurrentWeight.
   *
   * @param packageCurrentWeight the higher package current weight
   * @param packageCurrentPrice  the higher package current price
   * @param totalWeight          the totalWeight of the items
   * @param totalPrice           the totalPrice of the items
   * @return true if the packageCurrentPrice is equals to totalPrice and the totalWeight is lower than the packageCurrentWeight, false otherwise.
   */
  private boolean isSamePriceAndWeightsLower(BigDecimal packageCurrentWeight, BigDecimal packageCurrentPrice, BigDecimal totalWeight, BigDecimal totalPrice) {
    return packageCurrentPrice.compareTo(totalPrice) == 0 && packageCurrentWeight.compareTo(totalWeight) == 1;
  }

  /**
   * Verifies if the totalPrice is higher than the packageCurrentPrice.
   *
   * @param packageCurrentPrice the higher package current pricea
   * @param totalPrice          the totalPrice of the items
   * @return true if the totalPrice is higher than the packageCurrentPrice, false otherwise.
   */
  private boolean isPriceHigher(BigDecimal packageCurrentPrice, BigDecimal totalPrice) {
    return packageCurrentPrice.compareTo(totalPrice) == -1;
  }
}
