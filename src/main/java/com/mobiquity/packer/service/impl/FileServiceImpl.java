package com.mobiquity.packer.service.impl;

import com.mobiquity.packer.service.FileService;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

/**
 * The type File service.
 */
public class FileServiceImpl implements FileService {

  /**
   * Instantiates a new File service.
   */
  public FileServiceImpl() {
  }

  /**
   * Gets the lines of File from the filePath.
   *
   * @param filePath the file path
   * @return the lines
   * @throws IOException the io exception
   */
  @Override
  public List<String> getLines(String filePath) throws IOException {
    Path path = Paths.get(filePath);
    return Files.lines(path).collect(Collectors.toList());
  }
}
