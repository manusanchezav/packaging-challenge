package com.mobiquity.packer.service.impl;

import com.mobiquity.packer.exception.ValidationException;
import com.mobiquity.packer.mapper.PackageMapper;
import com.mobiquity.packer.model.Package;
import com.mobiquity.packer.service.FileService;
import com.mobiquity.packer.service.PackageService;
import com.mobiquity.packer.service.PackerService;
import com.mobiquity.packer.validator.Validator;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

/**
 * The type Packer service.
 */
public class PackerServiceImpl implements PackerService {

  private static final String LINE_BREAK = "\n";

  private FileService fileService;
  private Validator validator;
  private PackageMapper packageMapper;
  private PackageService packageService;

  /**
   * Instantiates a new Packer service.
   *
   * @param fileService    the file service
   * @param packageService the package service
   * @param validator      the validator
   * @param packageMapper  the package mapper
   */
  public PackerServiceImpl(final FileService fileService, final PackageService packageService, final Validator validator, final PackageMapper packageMapper) {
    this.fileService = fileService;
    this.packageService = packageService;
    this.validator = validator;
    this.packageMapper = packageMapper;
  }

  /**
   * Pack method that returns a string representation of the packaging.
   *
   * @param filePath the file path
   * @return the string
   * @throws ValidationException the validation exception
   * @throws IOException         the io exception
   */
  @Override
  public String pack(String filePath) throws ValidationException, IOException {
    List<String> lines = fileService.getLines(filePath);
    List<Package> packages = mapToPackages(lines);
    validate(packages);
    optimize(packages);
    return mapToString(packages);
  }

  /**
   * Validates the list of packages using the Validator provided.
   *
   * @param packages the list with all the packages
   * @throws ValidationException the validation exception
   */
  private void validate(List<Package> packages) throws ValidationException {
    for (Package aPackage : packages) {
      validator.validate(aPackage);
    }
  }

  /**
   * Optimize the list of packages using the Validator provided calling
   * the packageService to optimize each package.
   *
   * @param packages the list with all the packages
   */
  private void optimize(List<Package> packages) {
    packages.forEach(packageService::optimize);
  }

  /**
   * Maps the list of string of the packages calling the packageMapper
   * into a list of packages objects.
   *
   * @param lines the list of strings containing the information of each package
   */
  private List<Package> mapToPackages(List<String> lines) {
    return lines.stream()
        .map(packageMapper)
        .collect(Collectors.toList());
  }

  /**
   * Maps the list of packages calling the packageMapper
   * into a string representation.
   *
   * @param packages the list with all the packages
   */
  private String mapToString(List<Package> packages) {
    return packages.stream().map(packageMapper::applyToString).collect(Collectors.joining(LINE_BREAK));
  }
}