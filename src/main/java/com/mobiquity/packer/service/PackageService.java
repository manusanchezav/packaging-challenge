package com.mobiquity.packer.service;

import com.mobiquity.packer.model.Package;

/**
 * The interface Package service.
 */
public interface PackageService {

  /**
   * Optimize the package provided.
   *
   * @param aPackage the package
   */
  void optimize(Package aPackage);
}
