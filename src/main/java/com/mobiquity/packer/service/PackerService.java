package com.mobiquity.packer.service;

import com.mobiquity.packer.exception.ValidationException;

import java.io.IOException;

/**
 * The interface Packer service.
 */
public interface PackerService {

  /**
   * Pack method that returns a string representation of the packaging.
   *
   * @param filePath the file path
   * @return the string
   * @throws ValidationException the validation exception
   * @throws IOException         the io exception
   */
  String pack(String filePath) throws ValidationException, IOException;
}
