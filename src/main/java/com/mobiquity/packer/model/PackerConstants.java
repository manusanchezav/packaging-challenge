package com.mobiquity.packer.model;

/**
 * The type Packer constants.
 */
public final class PackerConstants {

  /**
   * The constant MAX_WEIGHT.
   */
  public static final Double MAX_WEIGHT = 100d;
  /**
   * The constant MAX_ITEMS.
   */
  public static final Integer MAX_ITEMS = 15;
  /**
   * The constant MAX_ITEM_WEIGHT.
   */
  public static final Double MAX_ITEM_WEIGHT = 100d;
  /**
   * The constant MAX_ITEM_PRICE.
   */
  public static final Double MAX_ITEM_PRICE = 100d;
}