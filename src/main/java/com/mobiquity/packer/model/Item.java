package com.mobiquity.packer.model;

/**
 * The type Item.
 */
public class Item {

  private Integer index;
  private Double weight;
  private Double price;

  /**
   * Instantiates a new Item.
   *
   * @param index  the index
   * @param weight the weight
   * @param price  the price
   */
  public Item(Integer index, Double weight, Double price) {
    this.index = index;
    this.weight = weight;
    this.price = price;
  }

  /**
   * Gets index.
   *
   * @return the index
   */
  public Integer getIndex() {
    return index;
  }

  /**
   * Sets index.
   *
   * @param index the index
   */
  public void setIndex(Integer index) {
    this.index = index;
  }

  /**
   * Gets weight.
   *
   * @return the weight
   */
  public Double getWeight() {
    return weight;
  }

  /**
   * Sets weight.
   *
   * @param weight the weight
   */
  public void setWeight(Double weight) {
    this.weight = weight;
  }

  /**
   * Gets price.
   *
   * @return the price
   */
  public Double getPrice() {
    return price;
  }

  /**
   * Sets price.
   *
   * @param price the price
   */
  public void setPrice(Double price) {
    this.price = price;
  }
}