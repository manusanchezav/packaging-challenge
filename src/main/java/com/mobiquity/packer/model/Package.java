package com.mobiquity.packer.model;

import java.util.List;

/**
 * The type Package.
 */
public class Package {

  private Double maxWeight;
  private List<Item> items;

  /**
   * Instantiates a new Package.
   *
   * @param maxWeight the max weight
   * @param items     the items
   */
  public Package(Double maxWeight, List<Item> items) {
    this.maxWeight = maxWeight;
    this.items = items;
  }

  /**
   * Gets max weight.
   *
   * @return the max weight
   */
  public Double getMaxWeight() {
    return maxWeight;
  }

  /**
   * Sets max weight.
   *
   * @param maxWeight the max weight
   */
  public void setMaxWeight(Double maxWeight) {
    this.maxWeight = maxWeight;
  }

  /**
   * Gets items.
   *
   * @return the items
   */
  public List<Item> getItems() {
    return items;
  }

  /**
   * Sets items.
   *
   * @param items the items
   */
  public void setItems(List<Item> items) {
    this.items = items;
  }
}
