package com.mobiquity.packer.validator;

import com.mobiquity.packer.AbstractTest;
import com.mobiquity.packer.exception.ValidationException;
import com.mobiquity.packer.model.Package;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.Collections;

import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class ValidatorCommandTest extends AbstractTest {

  @Mock
  private Validator maxItemsValidator;
  @Mock
  private Validator maxPackageWeightValidator;
  @Mock
  private Validator maxItemWeightAndPriceValidator;

  private Validator validatorCommand;

  @BeforeEach
  public void init() {
    validatorCommand = new ValidatorCommand(Arrays.asList(maxItemsValidator, maxPackageWeightValidator, maxItemWeightAndPriceValidator));
  }

  @Test
  public void validate_withValidPackageAndItems_callsValidators() throws ValidationException {
    Package pack = createNormalPackage(Collections.singletonList(createNormalItem()));
    validatorCommand.validate(pack);
    verify(maxItemsValidator).validate(pack);
    verify(maxPackageWeightValidator).validate(pack);
    verify(maxItemWeightAndPriceValidator).validate(pack);
  }

}
