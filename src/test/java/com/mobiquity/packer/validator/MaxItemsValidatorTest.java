package com.mobiquity.packer.validator;

import com.mobiquity.packer.AbstractTest;
import com.mobiquity.packer.exception.ValidationException;
import com.mobiquity.packer.model.Item;
import com.mobiquity.packer.model.PackerConstants;
import com.mobiquity.packer.validator.impl.MaxItemsValidator;
import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertThrows;

public class MaxItemsValidatorTest extends AbstractTest {

  private Validator maxItemsValidator = new MaxItemsValidator();

  @Test
  public void validate_withValidItemsQuantity_doesNotThrowException() throws ValidationException {
    maxItemsValidator.validate(createNormalPackage(Collections.singletonList(createNormalItem())));
  }

  @Test
  public void validate_withInvalidItemsQuantity_throwsValidationException() {
    List<Item> items = Collections.nCopies(PackerConstants.MAX_ITEMS + 1, createNormalItem());
    assertThrows(ValidationException.class, () -> maxItemsValidator.validate(createNormalPackage(items)));
  }

}
