package com.mobiquity.packer.validator;

import com.mobiquity.packer.AbstractTest;
import com.mobiquity.packer.exception.ValidationException;
import com.mobiquity.packer.model.Item;
import com.mobiquity.packer.model.PackerConstants;
import com.mobiquity.packer.validator.impl.MaxItemWeightAndPriceValidator;
import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertThrows;

public class MaxItemWeightAndPriceValidatorTest extends AbstractTest {

  private Validator maxItemWeightAndPriceValidator = new MaxItemWeightAndPriceValidator();

  @Test
  public void validate_withValidItemPriceAndWeight_doesNotThrowException() throws ValidationException {
    maxItemWeightAndPriceValidator.validate(createNormalPackage(Collections.singletonList(createNormalItem())));
  }

  @Test
  public void validate_withInvalidItemPrice_throwsValidationException() {
    List<Item> items = Collections.singletonList(new Item(ITEM_INDEX, ITEM_WEIGHT, PackerConstants.MAX_ITEM_PRICE + 1));
    assertThrows(ValidationException.class, () -> maxItemWeightAndPriceValidator.validate(createNormalPackage(items)));
  }

  @Test
  public void validate_withInvalidItemWeight_throwsValidationException() {
    List<Item> items = Collections.singletonList(new Item(ITEM_INDEX, PackerConstants.MAX_ITEM_WEIGHT + 1, ITEM_PRICE));
    assertThrows(ValidationException.class, () -> maxItemWeightAndPriceValidator.validate(createNormalPackage(items)));
  }


}
