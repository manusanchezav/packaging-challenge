package com.mobiquity.packer.validator;

import com.mobiquity.packer.AbstractTest;
import com.mobiquity.packer.exception.ValidationException;
import com.mobiquity.packer.model.Item;
import com.mobiquity.packer.model.Package;
import com.mobiquity.packer.model.PackerConstants;
import com.mobiquity.packer.validator.impl.MaxPackageWeightValidator;
import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertThrows;

public class MaxPackageWeightValidatorTest extends AbstractTest {

  private Validator maxPackageWeightValidator = new MaxPackageWeightValidator();

  @Test
  public void validate_withValidPackageWeight_doesNotThrowException() throws ValidationException {
    maxPackageWeightValidator.validate(createNormalPackage(Collections.singletonList(createNormalItem())));
  }

  @Test
  public void validate_withInvalidPackageWeight_throwsValidationException() {
    List<Item> items = Collections.singletonList(createNormalItem());
    assertThrows(ValidationException.class, () -> maxPackageWeightValidator.validate(new Package(PackerConstants.MAX_WEIGHT + 1, items)));
  }
}
