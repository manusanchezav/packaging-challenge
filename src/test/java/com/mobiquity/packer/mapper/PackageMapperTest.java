package com.mobiquity.packer.mapper;

import com.mobiquity.packer.AbstractTest;
import com.mobiquity.packer.model.Item;
import com.mobiquity.packer.model.Package;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;

public class PackageMapperTest extends AbstractTest {

  private static final String PACKAGE = "81 : (1,53.38,€45) (2,88.62,€98) (3,78.48,€3) (4,72.30,€76) (5,30.18,€9) (6,46.34,€48)";
  private static final String INVALID_PACKAGE = "255 | [A,S,D]";
  private static final Integer PACKAGE_ITEMS_QUANTITY = 6;
  private static final Double PACKAGE_WEIGHT = 81d;
  private static final Integer ITEM_TWO_INDEX = 2;
  private static final String PACKAGE_OUTPUT = "1,2";
  private static final String DASH = "-";

  private PackageMapper packageMapper = new PackageMapper();

  @Test
  public void apply_validPackageString_returnsPackage() {
    Package aPackage = packageMapper.apply(PACKAGE);
    assertEquals(PACKAGE_WEIGHT, aPackage.getMaxWeight());
    assertTrue(PACKAGE_ITEMS_QUANTITY == aPackage.getItems().size());
  }

  @Test
  public void applyToString_validPackage_returnsString() {
    Item itemOne = createNormalItem();
    Item itemTwo = createNormalItem();
    itemTwo.setIndex(ITEM_TWO_INDEX);
    Package aPackage = createNormalPackage(Arrays.asList(itemOne, itemTwo));
    String result = packageMapper.applyToString(aPackage);
    assertEquals(PACKAGE_OUTPUT, result);
  }

  @Test
  public void applyToString_EmptyPackageString_returnsDash() {
    Package aPackage = createNormalPackage(Collections.emptyList());
    String result = packageMapper.applyToString(aPackage);
    assertEquals(DASH, result);
  }

  @Test
  public void apply_invalidIPackageString_throwsNumberFormatException() {
    assertThrows(NumberFormatException.class, () -> packageMapper.apply(INVALID_PACKAGE));
  }
}