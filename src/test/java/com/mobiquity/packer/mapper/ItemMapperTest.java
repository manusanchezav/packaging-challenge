package com.mobiquity.packer.mapper;

import com.mobiquity.packer.AbstractTest;
import com.mobiquity.packer.model.Item;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class ItemMapperTest extends AbstractTest {

  private static final String ITEM = "(2,88.62,€98)";
  private static final String INVALID_ITEM = "[A,S,D]";
  private static final Integer ITEM_INDEX = 2;
  private static final Double ITEM_WEIGHT = 88.62d;
  private static final Double ITEM_PRICE = 98d;

  private ItemMapper itemMapper = new ItemMapper();

  @Test
  public void apply_validItemString_returnsItem() {
    Item item = itemMapper.apply(ITEM);
    assertEquals(ITEM_INDEX, item.getIndex());
    assertEquals(ITEM_WEIGHT, item.getWeight());
    assertEquals(ITEM_PRICE, item.getPrice());
  }

  @Test
  public void applyToString_validItem_returnsString() {
    Item item = createNormalItem();
    String index = itemMapper.applyToString(item);
    assertEquals(item.getIndex().toString(), index);
  }

  @Test
  public void apply_invalidItemString_throwsNumberFormatException() {
    assertThrows(NumberFormatException.class, () -> itemMapper.apply(INVALID_ITEM));
  }
}