package com.mobiquity.packer.model;

import com.mobiquity.packer.AbstractTest;
import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class PackageTest extends AbstractTest {

  private static final Double NEW_WEIGHT = 20d;

  private List<Item> items = Collections.singletonList(createNormalItem());

  @Test
  public void testCreatePackage() {
    Package aPackage = new Package(PACKAGE_WEIGHT, items);
    assertNotNull(aPackage);
  }

  @Test
  public void testGetters() {
    Package aPackage = createNormalPackage(items);
    assertEquals(PACKAGE_WEIGHT, aPackage.getMaxWeight());
    assertEquals(items, aPackage.getItems());
  }

  @Test
  public void testSetters() {
    Package aPackage = createNormalPackage(items);
    aPackage.setMaxWeight(NEW_WEIGHT);
    aPackage.setItems(Collections.emptyList());
    assertEquals(NEW_WEIGHT, aPackage.getMaxWeight());
    assertTrue(aPackage.getItems().isEmpty());
  }
}