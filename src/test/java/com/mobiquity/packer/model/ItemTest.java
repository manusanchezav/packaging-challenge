package com.mobiquity.packer.model;

import com.mobiquity.packer.AbstractTest;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class ItemTest extends AbstractTest {

  private static final Integer NEW_INDEX = 2;
  private static final Double NEW_WEIGHT = 20d;
  private static final Double NEW_PRICE = 70d;

  @Test
  public void testCreateItem() {
    Item item = new Item(ITEM_INDEX, ITEM_WEIGHT, ITEM_PRICE);
    assertNotNull(item);
  }

  @Test
  public void testGetters() {
    Item item = createNormalItem();
    assertEquals(ITEM_INDEX, item.getIndex());
    assertEquals(ITEM_PRICE, item.getPrice());
    assertEquals(ITEM_WEIGHT, item.getWeight());
  }

  @Test
  public void testSetters() {
    Item item = createNormalItem();
    item.setIndex(NEW_INDEX);
    item.setWeight(NEW_WEIGHT);
    item.setPrice(NEW_PRICE);
    assertEquals(NEW_INDEX, item.getIndex());
    assertEquals(NEW_WEIGHT, item.getWeight());
    assertEquals(NEW_PRICE, item.getPrice());
  }
}