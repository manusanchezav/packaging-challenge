package com.mobiquity.packer.factory;

import com.mobiquity.packer.validator.Validator;
import com.mobiquity.packer.validator.ValidatorCommand;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class ValidatorFactoryTest {

  @Test
  public void create_successfullyCreatesValidatorCommand() {
    Validator validator = ValidatorFactory.create();
    assertNotNull(validator);
    assertEquals(ValidatorCommand.class, validator.getClass());
  }
}
