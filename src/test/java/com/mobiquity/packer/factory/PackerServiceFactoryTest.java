package com.mobiquity.packer.factory;

import com.mobiquity.packer.service.PackerService;
import com.mobiquity.packer.service.impl.PackerServiceImpl;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class PackerServiceFactoryTest {

  @Test
  public void create_successfullyCreatesPackerServiceImpl() {
    PackerService packerService = PackerServiceFactory.create();
    assertNotNull(PackerServiceFactory.create());
    assertEquals(PackerServiceImpl.class, packerService.getClass());
  }
}
