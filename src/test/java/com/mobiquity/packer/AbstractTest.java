package com.mobiquity.packer;

import com.mobiquity.packer.model.Item;
import com.mobiquity.packer.model.Package;

import java.util.List;

public abstract class AbstractTest {

  protected static final String EXAMPLE_INPUT_FILE_PATH = "src/test/resources/example_input";
  protected static final String EXAMPLE_OUTPUT_FILE_PATH = "src/test/resources/example_output";
  protected static final String INVALID_PATH = "invalidPath";
  protected static final Integer ITEM_INDEX = 1;
  protected static final Double ITEM_WEIGHT = 88.62d;
  protected static final Double ITEM_PRICE = 90d;
  protected static final Double PACKAGE_WEIGHT = 90d;

  protected Item createNormalItem() {
    return new Item(ITEM_INDEX, ITEM_WEIGHT, ITEM_PRICE);
  }

  protected Package createNormalPackage(List<Item> items) {
    return new Package(PACKAGE_WEIGHT, items);
  }
}
