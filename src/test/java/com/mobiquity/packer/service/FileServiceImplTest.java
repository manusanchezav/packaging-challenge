package com.mobiquity.packer.service;

import com.mobiquity.packer.AbstractTest;
import com.mobiquity.packer.service.impl.FileServiceImpl;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class FileServiceImplTest extends AbstractTest {

  private static final String SECOND_LINE = "8 : (1,15.3,€34)";

  private FileService fileService = new FileServiceImpl();

  @Test
  public void getLines_withValidFile_doesNotThrowException() throws IOException {
    List<String> result = fileService.getLines(EXAMPLE_INPUT_FILE_PATH);
    assertEquals(SECOND_LINE, result.get(1));
  }

  @Test
  public void getLines_withInvalidFile_throwsValidationException() {
    assertThrows(IOException.class, () -> fileService.getLines(INVALID_PATH));
  }

}
