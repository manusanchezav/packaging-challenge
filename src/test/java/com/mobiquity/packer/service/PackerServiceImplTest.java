package com.mobiquity.packer.service;

import com.mobiquity.packer.AbstractTest;
import com.mobiquity.packer.exception.ValidationException;
import com.mobiquity.packer.mapper.PackageMapper;
import com.mobiquity.packer.model.Package;
import com.mobiquity.packer.service.impl.PackerServiceImpl;
import com.mobiquity.packer.validator.Validator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.IOException;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class PackerServiceImplTest extends AbstractTest {

  private static final String PACKAGE = "dummyPackage";
  private static final String PACK_RESULT = "dummyResult";

  @Mock
  private FileService fileService;
  @Mock
  private Validator validator;
  @Mock
  private PackageMapper packageMapper;
  @Mock
  private PackageService packageService;

  private PackerService packerService;
  private Package aPackage;

  @BeforeEach
  public void init() throws IOException {
    aPackage = createNormalPackage(Collections.singletonList(createNormalItem()));
    packerService = new PackerServiceImpl(fileService, packageService, validator, packageMapper);
  }

  @Test
  public void pack_withValidFilePath_returnsExpectedString() throws ValidationException, IOException {
    when(fileService.getLines(EXAMPLE_INPUT_FILE_PATH)).thenReturn(Collections.singletonList(PACKAGE));
    when(packageMapper.apply(any())).thenReturn(aPackage);
    when(packageMapper.applyToString(aPackage)).thenReturn(PACK_RESULT);

    String result = packerService.pack(EXAMPLE_INPUT_FILE_PATH);

    verify(fileService).getLines(EXAMPLE_INPUT_FILE_PATH);
    verify(packageMapper).apply(any());
    verify(validator).validate(aPackage);
    verify(packageService).optimize(aPackage);
    verify(packageMapper).applyToString(aPackage);
    assertEquals(PACK_RESULT, result);
  }

  @Test
  public void pack_withInvalidFilePath_throwsException() throws ValidationException, IOException {
    when(fileService.getLines(INVALID_PATH)).thenThrow(new IOException());

    assertThrows(IOException.class, () -> packerService.pack(INVALID_PATH));

    verify(packageMapper, never()).apply(any());
    verify(validator, never()).validate(aPackage);
    verify(packageService, never()).optimize(aPackage);
    verify(packageMapper, never()).applyToString(any());
  }

}
