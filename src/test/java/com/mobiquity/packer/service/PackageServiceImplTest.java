package com.mobiquity.packer.service;

import com.mobiquity.packer.AbstractTest;
import com.mobiquity.packer.model.Item;
import com.mobiquity.packer.model.Package;
import com.mobiquity.packer.service.impl.PackageServiceImpl;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class PackageServiceImplTest extends AbstractTest {

  private PackageService packageService = new PackageServiceImpl();

  @Test
  public void optimize_withValidItems_returnsOptimizedPackage() {
    Package aPackage = createPackageForOptimization();
    packageService.optimize(aPackage);
    assertTrue(aPackage.getItems().size() == 2);
    assertEquals(Integer.valueOf(8), aPackage.getItems().get(0).getIndex());
    assertEquals(Integer.valueOf(9), aPackage.getItems().get(1).getIndex());
  }

  @Test
  public void optimize_withNoItems_returnsOptimizedPackageWithNoItems() {
    Package aPackage = createNormalPackage(Collections.emptyList());
    packageService.optimize(aPackage);
    assertTrue(aPackage.getItems().isEmpty());
  }

  @Test
  public void optimize_withNoPossibleFittingItems_returnsOptimizedPackageWithNoItems() {
    Package aPackage = new Package(8d, Collections.singletonList(new Item(1, 15.3d, 34d)));
    packageService.optimize(aPackage);
    assertTrue(aPackage.getItems().isEmpty());
  }

  private Package createPackageForOptimization() {
    List<Item> items = new ArrayList<>();
    items.add(new Item(1, 90.72, 13d));
    items.add(new Item(2, 33.80, 40d));
    items.add(new Item(3, 43.15, 10d));
    items.add(new Item(4, 37.97, 16d));
    items.add(new Item(5, 46.81d, 36d));
    items.add(new Item(6, 48.77, 79d));
    items.add(new Item(7, 81.80, 45d));
    items.add(new Item(8, 19.36, 79d));
    items.add(new Item(9, 6.76d, 64d));
    return new Package(56d, items);
  }

}
