package com.mobiquity.packer;

import com.mobiquity.exception.APIException;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class PackerTest extends AbstractTest {

  private static final String MAX_PACKAGE_WEIGHT_INVALID_FILE_PATH = "src/test/resources/maxPackageWeight_invalid_input";
  private static final String MAX_ITEMS_INVALID_FILE_PATH = "src/test/resources/maxItems_invalid_input";
  private static final String MAX_ITEM_WEIGHT_INVALID_FILE_PATH = "src/test/resources/maxItemWeight_invalid_input";
  private static final String MAX_ITEM_PRICE_INVALID_FILE_PATH = "src/test/resources/maxItemPrice_invalid_input";
  private static final String WRONG_PATH = "src/test/resources/wrong/wrong_path";

  @Test
  public void pack_withValidInputFile_returnsOptimizedPackageAsString() throws APIException, IOException {
    String result = Packer.pack(EXAMPLE_INPUT_FILE_PATH);
    Path path = Paths.get(EXAMPLE_OUTPUT_FILE_PATH);
    String expectedOutput = Files.readString(path, StandardCharsets.UTF_8);
    assertEquals(expectedOutput, result);
  }

  @Test
  public void pack_withMaxPackageWeightInvalidInputFile_throwsAPIException() {
    assertThrows(APIException.class, () -> Packer.pack(MAX_PACKAGE_WEIGHT_INVALID_FILE_PATH));
  }

  @Test
  public void pack_withMaxItemsInvalidInputFile_throwsAPIException() {
    assertThrows(APIException.class, () -> Packer.pack(MAX_ITEMS_INVALID_FILE_PATH));
  }

  @Test
  public void pack_withMaxItemWeightInvalidInputFile_throwsAPIException() {
    assertThrows(APIException.class, () -> Packer.pack(MAX_ITEM_WEIGHT_INVALID_FILE_PATH));
  }

  @Test
  public void pack_withMaxItemPriceInvalidInputFile_throwsAPIException() {
    assertThrows(APIException.class, () -> Packer.pack(MAX_ITEM_PRICE_INVALID_FILE_PATH));
  }

  @Test
  public void pack_withWrongPath_throwsAPIException() {
    assertThrows(APIException.class, () -> Packer.pack(WRONG_PATH));
  }
}
